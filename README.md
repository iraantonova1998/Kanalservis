Test task from the company Kanalservis.
1. Login: react
2. Password: qwerty

## Technologies
 1) React
 2) Redux (redux-toolkit, thunk)
 3) TypeScript
 4) Styled Component
 5) Axious

## Getting Started

Clone this repository:

```
git clone https://github.com/IrinRer/Kanalservis.git
```

Install dependencies:

```
npm i
```

Runs the app in the development mode:

```
npm start
```
